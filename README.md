# Hide language select
This simple module hides the language select field on user accounts and provides 
a permission to make it visible for selected roles.

This might be a useful feature in a scenario where you have your drupal 
configured to support multiple languages and want your editors to change their 
backend language, but for all other visitors and users the language should not
be configurable.

## Post-Installation
- If you just want to hide the field, just enable the module - no further 
configuration required.
- If you like to make the language select field visible for certain users, just 
go to /admin/people/permissions and set the 'Show language select on user 
edit form' for each desired role.

## Additional Requirements
- User based language selection should be activated, otherwise this module has 
no effect.

## Similar modules
- Hidden Language https://www.drupal.org/project/hidden_language
This module allows site admins to hide languages from end-users. As opposed to 
disabling language content editors still, can translate content to that 
language. It's useful if you don't have the entire website translated.
Language access https://www.drupal.org/project/language_access
Restrict user access to locale languages using permission.
Useful for sites where multilingual content has not been fully prepared yet.